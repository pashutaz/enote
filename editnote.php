<?php
/**
 * Created by PhpStorm.
 * User: pashutaz
 * Date: 07.05.17
 * Time: 23:02
 */
session_start();
include "notes.php";
$headerAddress = "Location: /index.php";
$error = "";
$diaryTitle = '';
$diaryContent = '';


if ((array_key_exists('id', $_COOKIE) && isset($_COOKIE['id'])) && (!array_key_exists('id', $_SESSION) && !isset($_SESSION['id']))) {
    $_SESSION['id'] = $_COOKIE['id'];
}

if (!array_key_exists('idNote', $_SESSION)) {

    if (headers_sent() == false) {
        header($headerAddress);
        die();
    } else {
        $error .= "<p>headers have sent</p>";
        print_r($_SESSION);
    }

}

if ((array_key_exists('id', $_SESSION) && $_SESSION['id']) && (array_key_exists('idNote', $_SESSION) && $_SESSION['idNote'])) {
    $notes = $_SESSION['allNotes'];
//        print_r($_SESSION);

    $db = new Database();
    $query = "SELECT Notes.title,Notes.content FROM Notes WHERE Notes.idNote = '" . mysqli_real_escape_string($db->mysqli, $_SESSION['idNote']) . "' LIMIT 1";
    $row = mysqli_fetch_array($db->do_query($query));

    $diaryTitle = $row['title'];
    $diaryContent = $row['content'];

    if (array_key_exists('deleteNote', $_POST)){
        //        include "notes.php";
        // $notes[$_SESSION['idNote']]->delete();
        $db->do_query("DELETE FROM Notes WHERE Notes.idNote = '" . $_SESSION['idNote'] . "' ");
        if (headers_sent() == false) {
            header($headerAddress);
            die();
        } else {
            $error .= "<p>headers have sent</p>";
            print_r($_SESSION);
        }

    }elseif (array_key_exists('uploadFileButton', $_POST)){
        $target_file = 'uploads/' . $_SESSION['username'] . '/' . basename($_FILES["fileUpload"]["name"]);
        if (move_uploaded_file($_FILES["fileUpload"]["tmp_name"], $target_file)) {
            $error .= "<div class='alert alert-success' role='alert'><p>The file ". basename( $_FILES["fileUpload"]["name"]). " has been uploaded.</p></div>";
        } else {
            $error .= "<div class='alert alert-danger' role='alert'><p>Sorry, there was an error uploading your file.</p></div>";
        }
    }
}else{

    if (headers_sent() == false) {
        header($headerAddress);
        die();
    }else {
        $error .= "<p>headers have sent</p>";
        print_r($_SESSION);
    }

}
echo "<p class='hidden'></p>";
//  return errors
// if ($error) $error = "<div class='alert alert-warning' role='alert'>" . $error . "</div>";
?>

<?php include "components/header.php";?>

<?php include "components/navbar.php"; ?>

<div class="container-fluid">
  <?php if ($error) echo $error = "<div id='error'>$error</div>"; ?>

    <div class="row">

        <div class="col-md-9">
            <input class="form-control" id="title" name="diaryTitle" title="Your Title" placeholder="Title" type="text" value="<?php echo $diaryTitle; ?>"/>
            <!--    <textarea class="form-control" id="title" name="diaryTitle" rows="1" cols="164" style="width: 90%;font-size: 40px;  margin: 5px auto 5px;" title="Your Title" placeholder="Title">--><?php //echo $diaryTitle; ?><!--</textarea>-->
            <textarea class="form-control" id="diary" name="diaryText" title="Your Diary" placeholder="Your Note goes here"><?php echo $diaryContent; ?></textarea>
        </div>

        <div class="col-md-3">
            <form method="post" enctype="multipart/form-data">
                <input type="file" value="Choose file" name="fileUpload">
                <button class='btn btn-block btn-success my-2 my-sm-0' name='uploadFileButton' role="button">Upload</button>
            </form>
        </div>
    </div>

</div>

<?php include "components/footer.php";?>
