<?php
/**
 * Created by PhpStorm.
 * User: pashutaz
 * Date: 18.09.17
 * Time: 12:00
 */
session_start();
include "database/connection.php";

$msg = "";
$headerAddress = "Location: /index.php";

if (array_key_exists("code", $_GET)) {
    $query = "SELECT * FROM Users WHERE verification = '" . mysqli_real_escape_string($link, $_GET['code']) . "'";
    $result = mysqli_query($link, $query);
    $row = mysqli_fetch_array($result);

    if (mysqli_num_rows($result) == 1) {
        echo "<script>
            document.getElementById('email').disabled = true;
            document.getElementById('username').disabled = true;
        </script>";
        $_SESSION['id'] = $row['idUsers'];
        $_SESSION['username'] = $row['username'];
        $_SESSION['email'] = $row['email'];
    }
}

//checking cookie
if ((array_key_exists('id', $_COOKIE) && isset($_COOKIE['id'])) && (!array_key_exists('id', $_SESSION) && !isset($_SESSION['id']))) {

    $_SESSION['id'] = $_COOKIE['id'];

}

if (!array_key_exists('id', $_SESSION) && !isset($_SESSION['id'])) {

    if (headers_sent() === false) {
        header($headerAddress);
    } else {
        $msg .= "<p>Headers have sent</p>";
        print_r($_SESSION);
    }

}

//  saving profile info
if (array_key_exists('save', $_POST)) {

//    checking for errors
    if (!$_POST['email']) {

        $msg .= "<p>Email field cannot be empty</p>";

    } else {

        $query = "SELECT * FROM Users WHERE email = '" . mysqli_real_escape_string($link, $_POST['email']) . "' LIMIT 1";
        $result = mysqli_query($link, $query);
        $row = mysqli_fetch_array($result);

        if ($row['idUsers'] != $_SESSION['id'] && $row['email'] == $_POST['email']) {

            $msg .= "<p>This email already taken, try another</p>";

        }
    }
    if ($_POST['password'] != $_POST['password-check']) {
        $msg .= "<p>Passwords didn't match</p>";
    }

//  email data in
    if (!$msg) {

        //   password in
        if (!empty($_POST['password']) || !empty($_POST['password-check'])) {

            $hashedPassword = password_hash($_SESSION['id'] . $_POST['password'], PASSWORD_DEFAULT);
            $query = "UPDATE Users SET Users.username = '" . mysqli_real_escape_string($link, $_POST['username']) . "', Users.email = '" . mysqli_real_escape_string($link, $_POST['email']) . "', Users.password = '" . mysqli_real_escape_string($link, $hashedPassword) . "'  WHERE Users.idUsers = '" . $_SESSION['id'] . "' LIMIT 1";
            mysqli_query($link, $query) or die("Couldn't save changes");
            $_SESSION['username'] = $_POST['username'];
            $_SESSION['email'] = $_POST['email'];

        } else {

            $query = "UPDATE Users SET Users.username = '" . mysqli_real_escape_string($link, $_POST['username']) . "', Users.email = '" . mysqli_real_escape_string($link, $_POST['email']) . "' WHERE Users.idUsers = '" . $_SESSION['id'] . "' LIMIT 1";
            mysqli_query($link, $query) or die("Couldn't save changes");
            $_SESSION['username'] = $_POST['username'];
            $_SESSION['email'] = $_POST['email'];

        }
        $msg = '<div class="alert alert-success" role="alert"><p>Saved changes successfully!</p></div>';

    } else {

        $msg = '<div class="alert alert-danger" role="alert"><p>There were errors in form: </p>' . $msg . '</div>';

    }

}

include 'components/header.php'; ?>

<?php include "components/navbar.php"; ?>

    <div class="container">

        <h1>Profile Info</h1>

        <div id="error"><p><?php echo $msg; ?></p></div>

        <form id="profileInfo" method="post">
            <div class="form-group">
                <label for="username">Username</label>
                <input class="form-control" id="username" type="text" name="username" placeholder="new username"
                       value="<?php echo $_SESSION['username'] ?>">
            </div>

            <div class="form-group">
                <label for="email">Email address</label>
                <input class="form-control" id="email" type="email" name="email" placeholder="new email address"
                       value="<?php echo $_SESSION['email'] ?>" required>
            </div>

            <div class="tablet">
                <div class="form-group">
                    <label for="password">New password</label>
                    <input class="form-control" type="password" name="password" placeholder="new password">
                </div>

                <div class="form-group">
                    <label for="password">Confirm new password</label>
                    <input class="form-control" type="password" name="password-check" placeholder="repeat password">
                </div>
            </div>

            <div class="form-group" style="padding: 10px 10px">
                <button class="btn btn-success" name="save" role="button">Save changes</button>
            </div>
        </form>
    </div>

<?php include 'components/footer.php'; ?>
