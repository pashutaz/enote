<?php
/**
 * Created by PhpStorm.
 * User: pazay
 * Date: 28.10.2017
 * Time: 4:15
 */

function sendVerifyMail($email, $hash)
{
    $to = $email; // Send email to our user
    $subject = 'Enote Signup | Verification'; // Give the email a subject
    $message = '
    <p>Thanks for signing up!</p>
    <p>Your account has been created!</p>
    <p>Please click this link to activate your account:</p>
    <p><a href="http://pashutaz-com.stackstaging.com/activation.php/?code=' . $hash . '">Verify</a>.</p>'; // Our message above including the link

// Для отправки HTML-письма должен быть установлен заголовок Content-type
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Дополнительные заголовки
    $headers .= 'From:noreply@enote.com';

    mail($to, $subject, $message, $headers); // Send our email
}

function sendRestoreMail($email, $hash)
{
    $to = $email; // Send email to our user
    $subject = 'Enote restore password | Verification'; // Give the email a subject
    $message = '
    <p>You requested password restoration!</p>
    <p>Your account has been reset!</p>
    <p>Please click this link to restore your account:</p>
    <p><a href="http://pashutaz-com.stackstaging.com/profile.php/?code=' . $hash . '">Restore</a>.</p>'; // Our message above including the link

// Для отправки HTML-письма должен быть установлен заголовок Content-type
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Дополнительные заголовки
    $headers .= 'From:noreply@enote.com';

    mail($to, $subject, $message, $headers); // Send our email
}
//sendVerifyMail('pazaytsev@gmail.com',"ghbdgn");
//sendRestoreMail('pazaytsev@gmail.com',"ghbdgn");