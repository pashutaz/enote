<?php
/**
 * Created by PhpStorm.
 * User: pazay
 * Date: 28.10.2017
 * Time: 10:11
 */

include "components/header.php";
?>

<div class="container">

    <form id="restoreForm" method="post">
        <div class="form-group">
            <p>Restoring password</p>
            <label for="email">Email address</label>
            <input type="email" name="email" placeholder="email address" class="form-control">
        </div>
        <input type="submit" name="submit" value="Sign Up" class="btn btn-success">
    </form>

</div>

<?php include "components/footer.php"; ?>
