<?php
/**
 * Created by PhpStorm.
 * User: pazay
 * Date: 03.10.2017
 * Time: 17:34
 */

session_start();
include 'notes.php';
//include 'connection.php';
$headerAddress = "Location: /index.php";
$error = "";
$msg = "";

//user logout
if (array_key_exists('logout', $_GET)) {
    setcookie('id', '', time() - 1);
    $_COOKIE['id'] = '';
    // remove all session variables
    session_unset();
    // destroy the session
//    session_destroy();
}
//checking cookie
if ((array_key_exists('id', $_COOKIE) && !empty($_COOKIE['id'])) && (!array_key_exists('id', $_SESSION) && empty($_SESSION['id']))) {
    $_SESSION['id'] = $_COOKIE['id'];
}

