<!--navigation top bar-->
<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand" href="/" role="button">eNote</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item" style="margin-right: 5px;">
                <a class="btn btn-outline-warning btn-block my-2 my-sm-0" href='/index.php/?logout'>Logout</a>
            </li>

            <?php if ($_SERVER['PHP_SELF'] == '/allnotes.php'): ?>

                <li class="nav-item">
                    <a class="btn btn-outline-info btn-block" href="/profile.php" role="button">
                        <?= $_SESSION['username'] != null ? $_SESSION['username'] :  $_SESSION['email'] ?>
                    </a>
                </li>
                  <!--<li class="nav-item">
                <a class="nav-link" onclick="Share.vkontakte('http://pashutaz-com.stackstaging.com/usernote.php','Enote','https://goo.gl/VMfWSt','Store your ideas here')"><button class="btn btn-outline-success my-2 my-sm-0" id="shareVk" style="height: 38px; width: 100px; background: url('/images/vk_com.png') no-repeat center/cover;border: none"></button>
                </a>
                </li>-->
                </ul>

                <form class="form-inline my-2 my-lg-0" method="post">
                    <input class="btn btn-success btn-block my-2 my-sm-0" type="submit" name="submit" value="New Note" role="button">
                </form>

            <?php elseif ($_SERVER['PHP_SELF'] == '/editnote.php'): ?>

                    <li class="nav-item">
                        <a href="/index.php" class="btn btn-info btn-block my-2 my-sm-0" role="button">&#9665; Back</a>
                    </li>
                </ul>

                <form class="form-inline my-2 my-lg-0" method="post">
                    <button class='btn btn-block btn-danger my-2 my-sm-0' name='deleteNote' role="button">Delete</button>
                </form>

            <?php else: ?>

                    <li class="nav-item active">
                        <a href="/index.php" class="btn btn-info btn-block my-2 my-sm-0" role="button">&#9665; Back</a>
                    </li>
                </ul>

            <?php endif; ?>

    </div>
</nav>
