<?php
/**
 * Created by PhpStorm.
 * User: pashutaz
 * Date: 07.05.17
 * Time: 14:52
 */
session_start();
include "database/connection.php";
include "mail.php";
$error = "";
$headerAddress = "Location: /allnotes.php";

//  user logout
if (array_key_exists('logout', $_GET)){
    setcookie('id', '', time() - 60 * 60 * 24 * 365, '/');
//    $_COOKIE['id'] = '';
//    unset($_COOKIE);
    //  remove all session variables
    session_unset();
    //  destroy the session
    //    session_destroy();
}
//echo '<p>session:'.$_SESSION['id']."</p>";
//echo '<p>cookie:'.$_COOKIE['id'].'</p>';

//  user's session check
if ((array_key_exists('id', $_SESSION) and isset($_SESSION['id'])) or (array_key_exists('id', $_COOKIE) and isset($_COOKIE['id']))) {

    if (headers_sent() == false) {
        header($headerAddress);
        die();
    } else {
        $error .= "<p>header had sent</p>";
        print_r($_POST);
    }

}

//  form submitted
if (array_key_exists("submit",$_POST)){

    if (!$_POST['email']){
        $error .= "<p>Enter email</p>";
    }

    if (empty($_POST['password']) && array_key_exists("password", $_POST)) {
        $error .= "<p>Enter password</p>";
    }

    if (!$error){

        $query = "SELECT * FROM Users WHERE email = '" . mysqli_real_escape_string($link, $_POST['email']) . "' LIMIT 1";

        $result = mysqli_query($link, $query);
        $row = mysqli_fetch_array($result);
        $userID = $row['idUsers'];
        $email = mysqli_real_escape_string($link, $_POST['email']);

        //  restore account
        if ($_POST['login'] == 'restore') {

            if (mysqli_num_rows($result) == 1) {
                $verifyHash = md5($email . time());
                $query = "UPDATE `Users` SET  Users.`verification` = '" . $verifyHash . "' WHERE Users.`email` = " . $email . " LIMIT 1";
                mysqli_query($link, $query);
                sendRestoreMail($email, $verifyHash);
                $error .= "<p>We sent you email with restoration link!</p>";
            } else {
                $error .= "<p>We didn't find that user</p>";
            }

        } else //sign up
            if (mysqli_num_rows($result) != 1 and $_POST['login'] == '0') {
                if ($_POST['password'] == $_POST['password-check']) {

                    $verifyHash = password_hash($email . time(), PASSWORD_DEFAULT);
                    $hashedPassword = password_hash($userID . $_POST['password'], PASSWORD_DEFAULT);
                    $query = "INSERT INTO Users(email,password,verification) VALUES ('" . mysqli_real_escape_string($link, $_POST['email']) . "','" . mysqli_real_escape_string($link, $hashedPassword) . "', '" . $verifyHash . "')";

                    if (mysqli_query($link, $query)) {
                        $userID = mysqli_insert_id($link);
                        $hashedPassword = password_hash($userID . $_POST['password'], PASSWORD_DEFAULT);
                        $_SESSION['id'] = $userID;
                        $query = "UPDATE `Users` SET  Users.`password` = '" . $hashedPassword . "' WHERE Users.`idUsers` = " . $userID . " LIMIT 1";
                        if (array_key_exists("stayLoggedIn", $_POST)) {
                            setcookie('id', $userID, time() + 60 * 60 * 24 * 365, '/');
                        }
                        mysqli_query($link, $query);
                        sendVerifyMail($email, $verifyHash);

                        if (headers_sent() === false) {
                            header($headerAddress);
                            die();
                        } else {
                            $error .= "<p>header have sent</p>";
                            print_r($_POST);
                        }

                    } else {
                        $error .= "<p>Could not sign you up</p>";
                    }
                } else {
                    $error .= "<p>Password didn't match</p>";
                }

                //  login  email not found
            } elseif (mysqli_num_rows($result) != 1) {

                $error = "<p>There is no such email, please sign up first.</p>";

                //  login
            } elseif ($_POST['login'] == '1') {

                if (password_verify($userID . $_POST['password'], $row['password'])) {

                    $_SESSION['id'] = $userID;
                    if (array_key_exists("stayLoggedIn", $_POST)) {
                        setcookie('id', $userID, time() + 60 * 60 * 24 * 365, '/');
                    }

                    if (headers_sent() === false) {
                        header($headerAddress);
                        die();
                    } else {
                        $error .= "<p>headers have sent</p>";
                        print_r($_POST);
                    }
                } else {

                    $error .= "<p>Wrong password</p>";

                }

                //  sign up taken email
            } else {

                $error .= "<p>This email already taken, try to login</p>";

            }

    }

    //  return errors
    if ($error) $error = "<div class='alert alert-danger' role='alert'><p>There were errors in form: </p>" . $error . "</div>";

}
?>

<?php include "components/header.php" ?>

<div class="container">
    <h1>eNote</h1>
    <p><strong>Store your ideas here</strong></p>
    <div id="error"><p><?php echo $error; ?></p></div>

    <!--    log in    -->
    <form id="logInForm" method="post">

        <p>Log in using your Email below</p>
        <div class="form-group">
            <label for="email">Email address</label>
            <input class="form-control" type="email" name="email" placeholder="email address">
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input class="form-control" type="password" name="password" placeholder="password">
        </div>

        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="stayLoggedIn" value="1" checked="checked">
                Stay logged in?
            </label>
        </div>

        <div class="form-group">
            <input type="hidden" name="login" value="1">
            <input class="btn btn-success" type="submit" name="submit" value="Log In" role="button">
        </div>

        <div class="form-group">
            <div>
                <button class="btn btn-link toggleForms" type="button" role="button">Sign Up</button>
            </div>

            <div>
                <button class="btn btn-link toggleRestoreForm" type="button" role="button">Forgot password?</button>
            </div>
        </div>

    </form>


    <!--    sign up   -->
    <form class="hidden" id="signUpForm" method="post">

        <p>Interested? Sign Up!</p>
        <div class="form-group">
            <label for="email">Email address</label>
            <input class="form-control" type="email" name="email" placeholder="email address">
            <small class="form-text text-muted" id="emailHelp">We'll never share your email with anyone else.</small>
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input class="form-control" type="password" name="password" placeholder="password">
        </div>

        <div class="form-group">
            <label for="password-check">Confirm new password</label>
            <input class="form-control" type="password" name="password-check" placeholder="repeat password">
        </div>

        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="stayLoggedIn" value="1" checked="checked">
                Stay logged in?
            </label>
        </div>

        <div class="form-group">
            <input type="hidden" name="login" value="0">
            <input class="btn btn-success" type="submit" name="submit" value="Sign Up" role="button">
        </div>

        <div class="form-group">
            <button class="btn btn-link toggleForms" type="button" role="button">Back to login</button>
        </div>

    </form>


    <!--    restore password    -->
    <form class="hidden" id="restoreForm" method="post">

        <div class="form-group">
            <p>Restoring password</p>
            <label for="email">Email address</label>
            <input class="form-control" type="email" name="email" placeholder="email address">
        </div>

        <input type="hidden" name="login" value="restore">
        <input class="btn btn-success" type="submit" name="submit" value="Restore" role="button">

        <div class="form-group">
            <button class="btn btn-link toggleRestoreForm" type="button" role="button">Back to login</button>
        </div>

    </form>


</div>

<footer>
    <p style="margin-right: 10px">
        <small>Created by <a href="https://github.com/pashutaz">Pashutaz</a></small>
    </p>
</footer>

<?php include "components/footer.php" ?>
