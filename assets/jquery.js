/**
 * Created by pashutaz on 04.12.2017.
 */

 //removes 000webhosting banner
 $( document ).ready(function() {
     $('div').last().remove();
 });


 $('textarea#diary').each(function () {
     this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
 }).on('input', function () {
     this.style.height = '80%';
     this.style.height = (this.scrollHeight) + 'px';
 });
 //  меняет формы входа и регистрации
 $(".toggleForms").click(function () {

     $("#logInForm").slideToggle();
     $("#signUpForm").slideToggle();
 });
 //  форма восстановление пароля
 $(".toggleRestoreForm").click(function () {
     $("#logInForm").slideToggle();
     $("#restoreForm").slideToggle();
 });

 $("#error").click(function () {
     $("#error").slideToggle("slow");
 });

 //    автосохранение
 $("#diary").keyup(function () {

     $.ajax({
         method: "POST",
         url: "updatedatabase.php",
         data: {content: this.value}
     });
 });

 $("#title").keyup(function () {
     $.ajax({
         method: "POST",
         url: "updatedatabase.php",
         data: {title: this.value}
     });
 });

 //Share bar
 Share = {
     vkontakte: function (purl, ptitle, pimg, text) {
         var url = 'http://vkontakte.ru/share.php?';
         url += 'url=' + encodeURIComponent(purl);
         url += '&title=' + encodeURIComponent(ptitle);
         url += '&description=' + encodeURIComponent(text);
         url += '&image=' + encodeURIComponent(pimg);
         url += '&noparse=true';
         Share.popup(url);
     },
     popup: function (url) {
         window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
     }
 };

 $(document).ready(function () {

     $(window).scroll(function () {
         if ($(this).scrollTop() > 40) {
             $('#topBtn').fadeIn();
         }else {
             $('#topBtn').fadeOut();
         }
     });

     $('#topBtn').click(function () {
         $('html, body').animate({scrollTop: 0}, 800);
     });
 });
